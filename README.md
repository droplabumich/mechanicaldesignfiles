# Deep Robot Optical Perception Sphere
## Hardware Files

This repo contains the necessary models and drawings to manufacture the DROP-Sphere. Simple assembly prints are provided as PDFs.

Information on the DROP-Sphere can be found here: http://www.dropsphere.net/